package nl.voorth.swingexercise;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static nl.voorth.swingexercise.SwingExerciseApplication.*;
import static org.assertj.core.api.Assertions.*;


public class ToDoTest {
    @Test
    public void testLoad() throws IOException {
        assertThat(todos("/todo.yml"))
                .isNotNull()
                .hasSize(2);
    }

}
