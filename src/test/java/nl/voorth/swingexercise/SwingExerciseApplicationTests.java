package nl.voorth.swingexercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SwingExerciseApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    void testTodo() {
        new ToDo("Pink Panther", "To do, to do, to o, to do ,to do, to do, to doo", Priority.LOW);
    }

}
