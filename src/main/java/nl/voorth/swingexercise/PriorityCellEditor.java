package nl.voorth.swingexercise;

import javax.swing.*;
import javax.swing.table.TableCellEditor;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PriorityCellEditor extends AbstractCellEditor
  implements TableCellEditor, ActionListener
{

  private Priority country;

  public PriorityCellEditor() {
  }

  @Override
  public Object getCellEditorValue() {
    return this.country;
  }

  @Override
  public Component getTableCellEditorComponent(JTable table, Object value,
                                               boolean isSelected, int row, int column) {
    if (value instanceof Priority) {
      this.country = (Priority) value;
    }

    JComboBox<Priority> combo = new JComboBox<>(Priority.values());

    combo.setSelectedItem(country);
    combo.addActionListener(this);

    if (isSelected) {
      combo.setBackground(table.getSelectionBackground());
    } else {
      combo.setBackground(table.getSelectionForeground());
    }

    return combo;
  }

  @Override
  public void actionPerformed(ActionEvent event) {
    JComboBox<Priority> comboCountry = (JComboBox<Priority>) event.getSource();
    this.country = (Priority) comboCountry.getSelectedItem();
  }

}