package nl.voorth.swingexercise;

public enum Priority {
    LOW("low"),
    MEDIUM("medium"),
    HIGH("high");

    private final String description;

    Priority(String description) {
        this.description = description;
    }


}
