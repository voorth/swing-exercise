package nl.voorth.swingexercise;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import javax.swing.*;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.net.NoRouteToHostException;
import java.util.Collections;
import java.util.List;

import static java.awt.BorderLayout.*;
import static javax.swing.UIManager.*;
import static javax.swing.UIManager.setLookAndFeel;
import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

public class SwingExerciseApplication {

    private final JLabel label = new JLabel("To do:");
    private JTable tblTodo;
    private JScrollPane scrTodo;
    private final JButton btnNew = new JButton("New...");
    private final JButton btnDelete = new JButton("Delete");
    private final JPanel buttons = new JPanel();

    public static void main(String[] args) throws Exception
    {
        setLookAndFeel(getSystemLookAndFeelClassName());
        var app = new SwingExerciseApplication();
        SwingUtilities.invokeLater(app::run);
    }

    private void run() {
        var file = "/todo.yml";
        initTable(file);
        scrTodo = new JScrollPane(tblTodo);

        btnNew.addActionListener(this::onNew);
        btnDelete.addActionListener(this::onDelete);

        buttons.add(btnNew);
        buttons.add(btnDelete);

        var frame = new JFrame("To Do List");
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());
        frame.setPreferredSize(new Dimension(400, 200));

        frame.add(buttons, NORTH);
        frame.add(scrTodo, CENTER);
        frame.pack();
        frame.setVisible(true);
    }

    public void onNew(ActionEvent event) {
        System.out.println("Add new row");
        var model = (ToDoModel) tblTodo.getModel();
        var newRow = model.addRow();
        tblTodo.changeSelection(newRow, 0, false, false);
        tblTodo.requestFocus();
    }

    public void onDelete(ActionEvent event) {
        System.out.println("Delete selected rows");
        var selectedRows = tblTodo.getSelectedRows();
        var model = (ToDoModel) tblTodo.getModel();
        model.removeRows(selectedRows);
    }

    private void initTable(String file) {
        tblTodo = new JTable(new ToDoModel(todos(file)));

        var columnModel = tblTodo.getColumnModel();
        tblTodo.setDefaultEditor(Priority.class, new PriorityCellEditor());
    }

    static List<ToDo> todos(String file) {
        try {
            var resource = SwingExerciseApplication.class.getResource(file);
            var mapper = new ObjectMapper(new YAMLFactory());
            return mapper.readValue(resource, new TypeReference<List<ToDo>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
            return Collections.EMPTY_LIST;
        }
    }


}
