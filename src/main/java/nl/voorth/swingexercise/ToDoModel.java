package nl.voorth.swingexercise;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import java.util.Comparator;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.IntStream;

public class ToDoModel extends AbstractTableModel {
    private List<ToDo> todos;

    private static final String[] COLUMN_NAMES =  {"Name", "Description", "Priority"};
    private static final Class[] COLUMN_TYPES =  {String.class, String.class, Priority.class};

    @Override
    public String getColumnName(int column) {
        return COLUMN_NAMES[column];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return COLUMN_TYPES[columnIndex];
    }

    public ToDoModel(List<ToDo> todos)
    {
        System.out.println(todos);
        this.todos = todos;
    }

    @Override
    public int findColumn(String columnName) {
        return super.findColumn(columnName);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }

    @Override
    public void setValueAt(Object value, int row, int col) {
        var todo = todos.get(row);
        switch (col) {
            case 0: todo.setName((String)value);
            break;
            case 1: todo.setDescription((String)value);
            break;
            case 2: todo.setPriority((Priority)value);
            break;
        }
    }

    @Override
    public int getRowCount() {
        return todos.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int row, int col) {
        var todo = todos.get(row);
        switch (col) {
            case 0: return todo.getName();
            case 1: return todo.getDescription();
            case 2: return todo.getPriority();
            default: return null;
        }
    }

    public int addRow() {
       todos.add(new ToDo("", "", Priority.LOW)) ;
       fireTableStructureChanged();
       return todos.size()-1;
    }

    public void removeRows(int... rows) {
        IntStream.of(rows)
          .boxed()
          .sorted(Comparator.reverseOrder())
          .mapToInt(Integer::intValue)
          .forEach(i -> todos.remove(i));
        fireTableStructureChanged();
    }
}
